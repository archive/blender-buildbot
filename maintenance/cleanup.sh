#!/bin/sh

ROOT="/data/buildbot-master/"
DIRS="linux_glibc211_i686_cmake
linux_glibc211_x86_64_cmake
linux_glibc219_i686_cmake
linux_glibc219_x86_64_cmake
mac_x86_64_10_6_cmake
win32_cmake_vc2013
win64_cmake_vc2013
win64_cmake_vc2015
uploaded"

for DIR in $DIRS; do
  find "${ROOT}/$DIR" -type f -mtime +14 -exec rm -f {} \;
done

