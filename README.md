Blender Buildbot
================

This repo contains the theme, CSS, HTML and public configurations of Blender's
[builder.blender.org](https://builder.blender.org/) web site.


Running locally
---------------

- `sudo apt-get install buildbot` or `pip install buildbot<0.9`
- `cp master_private_template.py master_private.py` and edit the file.
- `git submodule init`
- `git submodule update`


Changing CSS stuff
------------------

- `sudo npm install -g grunt` -- this installs to `/usr/local/lib/node_modules` and
  `/usr/local/bin`
- `npm install` -- this installs to `./node_modules`

After editing `public_html/assets/sass/main.sass`, run `grunt`.
