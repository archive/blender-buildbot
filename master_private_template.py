workers = [
    {'name': 'linux_something_something',
     'password': 'something',
     'platform': 'linux_centos7',
     'platform_short': 'linux'},
    {'name': 'macOS_10_15_something_something',
     'password': 'something',
     'platform': 'macOS_10_15',
     'platform_short': 'macos'},
    {'name': 'macOS_10_13_something_something',
     'password': 'something',
     'platform': 'macOS_10_13',
     'platform_short': 'macos'},
    {'name': 'windows_something_something',
     'password': 'something',
     'platform': 'windows',
     'platform_short': 'windows'}
]
